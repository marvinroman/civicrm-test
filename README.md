# Docker Setup for Drupal & CiviCRM


* [Requirements](#requirements)  
* [Deploy](#deploy)  
  * [Create MariaDB Configuration File](#create-mariadb-configuration-file)  
  * [Start The Dockers](#start-the-dockers)  
  * [Install Drupal 9](#install-drupal-9)  
  * [Install CiviCRM](#install-civicrm)  

## Requirements

* [Install Docker Engine](https://docs.docker.com/engine/install/).  
* [Install Docker Compose](https://docs.docker.com/compose/install/).  

## Deploy

### Create MariaDB Configuration File
In order to have the MariaDB be setup with credentials you will want to populate the following file `.mariadb_conf` with credentials first.  
```bash
MYSQL_ROOT_PASSWORD=
MYSQL_DATABASE=
MYSQL_USER=
MYSQL_PASSWORD=
```  

### Start The Dockers
You will now be able to deploy the dockers using the `docker-compose.yml` file using the following command.  
```bash
docker-compose up -d
```  

### Install Drupal 9
1. You can now install Drupal within the `iedsa_site` container. First you will want to enter the container. You will be entering as the user `www-data` so that you won't have to adjust ownership after install.    
```bash
docker exec -it -u www-data iedsa_site bash
```  
2. You will be brought into the folder `/var/www/html` which will be the root Drupal directory.  
3. From there you can use composer to install Drupal into the current directory with the following command.  
```bash
composer create-project drupal/recommended-project .
```  
4. Once composer completes the installation you will want to complete the Drupal installation within your browser by visiting [http://localhost](http://localhost), but don't yet exit the container.  
   * If you have another server already running on port **80** then you will want to change the port for the site by editing the `docker-compose.yml` file line with `"80:80"` to something like `"8080:80"` which would make the new port **8080**.  
5. When you get to the **Set up database** portion you will enter the values that you put into the file `.mariadb_conf` file. 
   1. Open up **Advanced Option** and change **Host** from `localhost` to `iedsa_data` which is resolved on the docker network to the MariaDB container.  

### Install CiviCRM

1. If you are not still inside the container from the Drupal installation then re-enter the container using the following command.   
```bash
docker exec -it -u www-data iedsa_site bash
```  
2. This will bring you to the folder `/var/www/html` where you will run the following commands to install CiviCRM.  
```bash
composer config extra.enable-patching true
composer config minimum-stability dev
composer require civicrm/civicrm-{core,packages,drupal-8}:'~5.42'
```  
3. You will want to change the site folder to be writable so that the UI can install CiviCRM using the following command.  
```bash
chmod 755 web/sites/default/
```  
4. Once the installation completes you can install the CiviCRM Core from the command line with the following command or from the UI under the **Extends** tab.  
```bash
cv core:install --cms-base-url="http://localhost" --lang="en_US"
```  